package router

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func createMockAlias() *alias {
	return &alias{
		"id", "some/path", 1,
	}
}

func Test_NewRouteAlias(t *testing.T) {
	id := "someID"
	uri := "some/path"
	len := 2
	expected := &alias{id, uri, len}
	assert.Equal(t, expected, NewRouteAlias(id, uri, len))
}

func Test_GetID(t *testing.T) {
	alias := createMockAlias()
	assert.Equal(t, "id", alias.GetID())
}

func Test_GetURI(t *testing.T) {
	alias := createMockAlias()
	assert.Equal(t, "some/path", alias.GetURI())
}

func Test_GetArgCount(t *testing.T) {
	alias := createMockAlias()
	assert.Equal(t, 1, alias.GetArgCount())
}
